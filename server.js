//version inicial

let express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
let path = require('path');
let requestjson = require('request-json');
const urlClientesMlab ="https://api.mlab.com/api/1/databases/ahernandez/collections/Pensionados?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&s";
const urlBeneficiosMlab ="https://api.mlab.com/api/1/databases/ahernandez/collections/Beneficios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&s";
const urlCiudadMlab ="https://restcountries.eu/rest/v2/name/Mexico?fields=name;capital";

var urlMLabRaiz="https://api.mlab.com/api/1/databases/ahernandez/collections/";
var apiKey="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;
var bcrypt = require('bcrypt');

let clientesMLab = requestjson.createClient(urlClientesMlab);
let beneficiosMLab = requestjson.createClient(urlBeneficiosMlab);
let ciudadMLab = requestjson.createClient(urlCiudadMlab);
let movimientosJSON = require('./movimientosv2.json');

let bodyparse = require('body-parser');
app.use(bodyparse.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-control-Allow-Header", "Origin, X-Requested-With,Content-Type, Accept");
  next();
});


app.listen(port);
console.log(`http://localhost:${port}`);
console.log('todo list RESTful API server started on: ' + port);


app.get("/V01/Clientes",getclientesAPI);
app.get("/V01/Ciudad",getCiudadAPI);


app.get('/v2/beneficios', function(req, res) {
   //console.log("Back de Beneficios");
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

   var emailglobal = req.headers.emailg;
   var query = 'q={"email":"'+ emailglobal + '"}';
   //console.log(query);

   clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Beneficios?" + apiKey + "&" + query);
   //console.log(urlMLabRaiz + "/Beneficios?" + apiKey + "&" + query);
   clienteMLabRaiz.get('', function(err, resM, body) {
   if (err) {
     console.log(body); }
   else {
     res.send(body);
     }
   })
 })



function getCiudadAPI(req, res){
  //console.log("Back de Ciudad");
  ciudadMLab.get('',function(err,resM,body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
}



function getclientesAPI(req, res){
clientesMLab.get('',function(err,resM,body){
  if(err){
    console.log(body);
  }else{
    res.send(body);
  }
})
}


function createClient(req, res){
  clientesMLab.post('',req.body,function(err, resM, body){
    res.send(body);
  })
}

//Este Login funciona correctamente sin bcrypt
/*
app.post('/v2/Login', function(req, res){
  var email = req.body.email;
  var password = req.body.password;
  var query ='q={"email":"' + email +'", "password":"' + password + '"}';
  console.log (query);

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Pensionados?" + apiKey + "&" + query)

  console.log(clienteMLabRaiz);
  console.log(urlMLabRaiz + "Pensionados?" + apiKey + "&" + query);


  clienteMLabRaiz.get('', function(err,resM, body) {
    if (!err){
      if (body.length==1){
        res.status(200).send('Usuario logado');
      }else  {
        res.status(404).send('Usuario Inexistente');
      }
    }
});
})
*/


app.post('/v2/Login', function(req, res){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  var email = req.body.email;
  var password = req.body.password;
  var query ='q={"email":"' + email +'"}';
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Pensionados?" + apiKey + "&" + query)

  clienteMLabRaiz.get('', function(err,resM, body) {

    if (!err) {
        if (body.length > 0) { //Login ok
         //console.log ('Login ok');
         //console.log('pass ' + password + ' ' + body[0].password);
         bcrypt.compare(password, body[0].password, function(err, ok) {
           if (ok) {
             res.status(200).send('Usuario logado correctamente');
           } else {
             res.status(404).send('Password no encontrado');
           }
          });
        } else {
         res.status(404).send('Usuario no encontrado');
        }
      } else {
        console.log(body);
      }
    })
})



//guardar nuevo Pensionado
app.post('/v2/guardar', function(req, res){
  var nombre = req.body.nombre;
  var ap = req.body.ap;
  var am = req.body.am;
  var email = req.body.email;
  var password = req.body.password;

  var query ='q={"nombre":"' + nombre +'", "ap":"' + ap + '", "am": "' + am + '", "beneficio":"' + email + '","importe":' + password + '}';

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Pensionados?" + apiKey + "&" + query)

  clientesMLab.post('', req.body, function(err, resM, body){
    if (!err){
        res.status(200).send('Pensionado Guardado');
      }else  {
        res.status(404).send('El Pensionado no se Guardo');
      }

});
})


//guardar nuevo Pensionado
app.post('/v2/guardarbcrypt', function(req, res){

  var nombre = req.body.nombre;
  var ap = req.body.ap;
  var am = req.body.am;
  var email = req.body.email;
  var passwordbcrypt = req.body.password;
  var BCRYPT_SALT_ROUNDS = 12;

   bcrypt.hash(passwordbcrypt, BCRYPT_SALT_ROUNDS)
     .then(function(hashedPasswordbcrypt) {
         //var query ='q={"nombre":"' + nombre +'", "ap":"' + ap + '", "am": "' + am + '", "email":"' + email + '","password":"' + hashedPassword + '"}';
         req.body.password=hashedPasswordbcrypt;
         var queryguarda ='q={"nombre":"' + nombre +'", "ap":"' + ap + '", "am": "' + am + '", "email":"' + email + '","password":"' + hashedPasswordbcrypt + '"}';

         clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Pensionados?" + apiKey + "&" + queryguarda)
         clientesMLab.post('', req.body, function(err, resM, body){
           //console.log(req.body);
           if (!err){
               res.status(200).send('Pensionado Guardado');
             }else  {
               res.status(404).send('El Pensionado no se Guardo');
             }
         });
     })
 })
